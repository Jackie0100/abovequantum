﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControlScheme : ScriptableObject
{
    Dictionary<string, KeyCode> Controls { get; set; }
    public KeyCode Forward { get; set; }
    public KeyCode Backward { get; set; }
    public KeyCode Left { get; set; }
    public KeyCode Right { get; set; }
    public KeyCode Jump { get; set; }
    public KeyCode Interact { get; set; }
    public KeyCode Pause { get; set; }
    public KeyCode Sprint { get; set; }

    public ControlScheme()
    {
    }

    public void ResetControls()
    {
        Controls = new Dictionary<string, KeyCode>()
        {
            {  "Forward", KeyCode.W },
        };
    }

    public void SaveControls()
    {

    }

    public void LoadControls()
    {

    }
}
